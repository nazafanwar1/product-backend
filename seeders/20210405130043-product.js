'use strict';
const faker = require("faker");
const Product = require("../models").Product;
const allProducts = require("../seeders-data/products");

const products = [...Array(400)].map(user => (
  {
    title: faker.commerce.productName(),
    description: faker.commerce.productDescription(),
    price: faker.commerce.price(),
    discountedPrice: faker.commerce.price(),
    firstImageUrl: faker.image.business(),
    secondImageUrl: faker.image.cats(),
    thirdImageUrl: faker.image.imageUrl(),
    fourthImageUrl: faker.image.fashion(),
    createdAt: new Date(),
    updatedAt: new Date(),

  }
));

module.exports = {
  up: async (queryInterface, Sequelize) => {

   return queryInterface.bulkInsert("Products", products, {});
  },

  down: queryInterface => queryInterface.bulkDelete("Products", null, {}),
};
