const express = require('express');
const http = require("http");
const router = require('./routes');
const corsMiddleware = require('./middlewares/cors');
const errorsMiddleware = require('./middlewares/errors');

const port = parseInt(process.env.PORT, 10) || 8000;

const app = express();
const server = http.createServer(app);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(corsMiddleware);
app.use(router);
app.use(errorsMiddleware);

app.set("port", port);

// server.listen(port);
module.exports = app;
