const { Router } = require("express");

const controller = require("../controller").product;
const { objHas } = require("../helpers/objectHelper");

const router = Router();

router.get('/', async (req, res, next) => {
    try {
      let products = [];
      if (objHas(req.query, "search")) {
        products = await controller.listByTitle(
          req.query.search,
          parseInt(req.query.offset),
          parseInt(req.query.limit)
        );
      } else {
        products = await controller.listAll(
          req.query,
          parseInt(req.query.offset),
          parseInt(req.query.limit)
        );
      }

      return res.status(200).send(products);
    } catch (error) {
      return next(error);
    }
});

router.get("/:id", async(req, res, next) => {
  try {
    const { id } = req.params;
    const product = await controller.getById(id);
    return res.status(200).send(product);
  } catch(error) {
    return next(error);
  }
})

router.post("/", async(req, res, next) => {
  try {
    const product = await controller.create(req.body);
    return res.status(201).send(product);
  } catch (error) {
    return next(error);
  }
})

router.put("/:id", async(req, res, next) => {
  try {
    const { id } = req.params;
    const product = await controller.update(id, req.body);
    return res.status(200).send(product);
  } catch (error) {
    return next(error);
  }
})

router.delete("/:id", async(req, res, next) => {
  try {
    const { id } = req.params;
    const product = await controller.remove(id);
    return res.status(200).send(product);
  } catch (error) {
    return next(error);
  }
})


module.exports = router;