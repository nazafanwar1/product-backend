const axios = require("axios");
const chai = require("chai");
const chaiHttp = require("chai-http");
const app = require("../app");

const should = chai.should();

chai.use(chaiHttp);

describe('Products', () => {
  describe('/GET product', () => {
    it('passing no query string, it should get all products', (done) => {
      chai.request(app)
        .get('/products?limit=10&offset=0')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.count.should.be.eql(400);
          done();
        });
    });
    it('passing query string (with 0 occurrences), it should get no products', (done) => {
      chai.request(app)
        .get('/products?limit=10&offset=0&search=foo')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.count.should.be.eql(0);
          done();
        });
    });
    it('passing wrong query string, it should not return any products', (done) => {
      chai.request(app)
        .get('/products?limit=20&offset=0&search=Nazaf')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.count.should.be.eql(0);
          done();
        });
    });
    it('passing query string (with occurrences), it should get the expected products', (done) => {
      chai.request(app)
        .get('/products?limit=10&offset=10&search=Rustic')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.count.should.be.eql(21);
          done();
        });
    });
    it('passing query string to test pagination, it should get the expected products and count ', (done) => {
      chai.request(app)
        .get('/products?limit=10&offset=10&search=Rustic')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          (res.body.rows.length).should.be.eql(10);
          done();
        });
    });
    it('passing query string to test pagination of last page for a specific item', (done) => {
      chai.request(app)
        .get('/products?limit=10&offset=20&search=Rustic')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          (res.body.rows.length).should.be.eql(1);
          done();
        });
    });
  });
});
