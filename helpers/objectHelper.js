function objHas(obj, key) {
  if (Array.isArray(key)) {
    for (let i = 0; i < key.length; i += 1) {
      if (!Object.prototype.hasOwnProperty.call(obj, key[i])) return false;
    }
    return true;
  }
  return Object.prototype.hasOwnProperty.call(obj, key);
}

module.exports = {
  objHas,
};
