const procedures = [
    {
        title: 'First Test',
        description: 'First Lençol description',
    },
    {
        title: 'Second Test',
        description: 'Second Lençol description',
    },
    {
        title: 'Kit de cama',
        description: 'Kit de cama - Classic solteiro extra',
    },
    {
        title: 'Kit de cama 210 fios',
        description: 'Kit de cama 210 fios - Classic solteiro extra',
    },
    {
        title: 'Kit de cama 210 fios',
        description: 'Kit de cama 210 fios - Classic solteiro extra',
    },
    {
        title: 'Lencol Avulso',
        description: 'Lencol Avulso - Kit de cama 210 fios classic casal extra',
    },
    {
        title: 'Lencol Avulso queen',
        description: 'Lencol Avulso - Queen',
    },
    {
        title: 'Lencol Avulso Casal',
        description: 'Lencol Avulso - Casal',
    },
    {
        title: 'Lencol Avulso queen casal',
        description: 'Lencol Avulso - Queen casal',
    },
    {
        title: 'Lencol Avulso solteiro',
        description: 'Lencol Avulso - solteiro',
    },
    {
        title: 'Lencol Avulso',
        description: 'Lencol Avulso - elastico',
    },
    {
        title: 'Lencol Avulso',
        description: 'Lencol Avulso - Sem elastico',
    },
    {
        title: 'Travesseiro',
        description: 'Travesseiro pequeno',
    },
    {
        title: 'Travesseiro Protetor',
        description: 'Protetor de Travesseiro',
    },
    {
        title: 'Travesseiro',
        description: 'Travesseiro medio',
    },
    {
        title: 'Travesseiro',
        description: 'Travesseiro grande',
    },
    {
        title: 'Roupa da cama',
        description: 'Roupa da cama solteiro',
    },
    {
        title: 'Roupa da cama',
        description: 'Roupa da cama solteiro elastico',
    },
    {
        title: 'Roupa da cama',
        description: 'Roupa da cama Casal elastico',
    },
    {
        title: 'Roupa da cama',
        description: 'Roupa da cama Casal elastico',
    },
    {
        title: 'Roupa da mesa',
        description: 'Roupa da mesa com elastico',
    },
    {
        title: 'Roupa da mesa',
        description: 'Roupa da mesa sem elastico',
    },
    {
        title: 'Cobertor',
        description: 'Cobertor silk solteiro',
    },
    {
        title: 'Cobertor',
        description: 'Cobertor silk casal',
    },
    {
        title: 'Cobertor',
        description: 'Cobertor silk casal queen size',
    },
    {
        title: 'Cobertor',
        description: 'Cobertor cotton solteiro',
    },
    {
        title: 'Cobertor',
        description: 'Cobertor cotton casal',
    },
    {
        title: 'Cobertor',
        description: 'Cobertor cotton casal queen size',
    },
    {
        title: 'Edredom',
        description: 'Edredom solteiro',
    },
    {
        title: 'Edredom',
        description: 'Edredom casal',
    },
    {
        title: 'Edredom',
        description: 'Edredom casal queen size',
    },
    {
        title: 'Edredom',
        description: 'Edredom infantil',
    },
    {
        title: 'Edredom',
        description: 'Edredom Duplace face aurora',
    },
    {
        title: 'Edredom',
        description: 'Edredom Soft touch',
    },
];
  

module.exports = procedures;
  