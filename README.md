# README #

This is a simple backend service using Node.js, express etc. to serve the data to a frontend service
in order to display and toggle around.

### What is this repository for? ###

* This backend service can be run locally by uncommenting ```server.listen(port);``` 
  in ```app.js```
    which exists at root. It's currently commented and exported as a module 
    ```module.exports = app``` in order to be deployed as a lambda function over AWS

### How do I get set up? ###

* Summary of set up - 
    The setup is pretty simple where there is a controller which does CRUD operation to a mysql table based on `AWS RDS` and there is a route that defines the endpoints to interact with the controller
* Configuration - 
    To configure the repository and packages, do  
    `git clone https://nazafanwar1@bitbucket.org/nazafanwar1/product-backend.git`  
    `cd product-backend`  
    and `npm i` as either dev dependency or as per your use
* Database configuration - 
    In order to keep it simple, I didn't use docker containers for each services and database but created a database instance on `RDS` itself which can be used here as our primary data. There is a seeders to preload some data and in case it's required to add more data, `products.js` whcich is in `seeders-data` directory can be edited and added more data and then run as `npm run seeders` to load the same
* Run Local - To run locally make sure the server is listening to a part as mentioned in the summary and run `npm run start:dev`
* Test - To run the test make sure server is listening to correct port in `app.js` and run  
`npm run test`
* Deployment instructions - 
    For deployment `claudia.js` has been used
### Who do I talk to? ###

* Repo owner or admin