const { Op } = require("sequelize");
const Product = require("../models").Product;

module.exports = {
    create: async(obj) => {
      const product = await Product.create({
        ...obj
      });

      return product;
    },

    listAll: async(query, offset, limit) => {
      const products = await Product.findAndCountAll({ offset: offset, limit: limit });
      
      return products;
    },

    listByTitle: async(search, offset, limit) => {
      const products = await Product.findAndCountAll({
        where: {
          [Op.or]: [
            { title: { [Op.like]: `%${search}%` } },
          ],
        },
        offset,
        limit,
      });

      return products;
    },

    getById: id => {
      return Product.findByPk(id);
    },

    update: async(id, obj) => {
      await Product.update(obj, { where: { id } });

      return Product.findByPk(id);
    },

    remove: async(id) => {
      const product = await Product.findByPk(id);
      
      return product.destroy();
    }
    
  };